package org.volatilecat.htmldiff;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.stream.Collectors;

public class HtmlAnalyzer {

    public Element findMostSimilarElement(Element sample, Document document) {
        Element parent = findNearestParent(sample, document);
        return search(sample, parent).element;
    }

    public String buildPathToElement(Element element) {
        StringBuilder builder = new StringBuilder();
        Deque<Element> hierarchy = buildHierarchy(element);

        for (Element currentElement : hierarchy) {
            builder.append(currentElement.tagName());

            Element parent = currentElement.parent();

            if (parent != null) {
                List<Element> sameTypeSiblings = parent.children().stream()
                        .filter(e -> e.tagName().equals(currentElement.tagName()))
                        .collect(Collectors.toList());

                if (sameTypeSiblings.size() > 1) {
                    int index = sameTypeSiblings.indexOf(currentElement);
                    builder.append("[").append(index + 1).append("]");
                }
            }

            if (currentElement != element) {
                builder.append(" > ");
            }
        }

        return builder.toString();
    }

    private SimilarElement search(Element sample, Element parent) {
        Element currentElement = parent;
        int distance = 0;

        SimilarElement candidate = searchRecursively(sample, currentElement, distance);

        do {
            candidate = candidate.mostSimilar(getSimilarity(sample, currentElement, distance));

            distance += 1;

            for (Element sibling : currentElement.siblingElements()) {
                candidate = candidate.mostSimilar(searchRecursively(sample, sibling, distance));
            }

            currentElement = currentElement.parent();
        } while (currentElement != null);

        return candidate;
    }

    private SimilarElement getSimilarity(Element sample, Element element, int distance) {
        int similarity = compareElements(sample, element) - distance;
        return new SimilarElement(element, similarity);
    }

    private SimilarElement searchRecursively(Element sample, Element parent, int distance) {
        SimilarElement candidate = getSimilarity(sample, parent, distance);

        for (Element child : parent.children()) {
            candidate = candidate.mostSimilar(searchRecursively(sample, child, distance + 1));
        }

        return candidate;
    }

    private int compareElements(Element sample, Element element) {
        int similarity = 0;

        if (sample.tagName().equals(element.tagName())) {
            similarity += 1;
        }

        if (sample.ownText().equals(element.ownText())) {
            similarity += 1;
        }

        return similarity + compareAttributes(sample, element);
    }

    private int compareAttributes(Element sample, Element element) {
        int similarity = 0;

        for (Attribute attribute : element.attributes()) {
            String key = attribute.getKey();

            if (!sample.hasAttr(key)) {
                similarity -= 1;
            } else if (sample.attr(key).equals(element.attr(key))) {
                similarity += 1;
            }
        }

        return similarity;
    }

    private Deque<Element> buildHierarchy(Element element) {
        Deque<Element> hierarchy = new ArrayDeque<>();
        Element parent = element.parent();
        while (parent != null) {
            hierarchy.addFirst(parent);
            parent = parent.parent();
        }
        hierarchy.addLast(element);
        return hierarchy;
    }

    /**
     * Returns the closest parent of the `sample` element which exists in the `document`.
     */
    private Element findNearestParent(Element sample, Document document) {
        Deque<Element> hierarchy = buildHierarchy(sample);
        Element nearestParent = document;
        hierarchy.pollFirst();
        hierarchy.pollLast();

        for (Element originElement : hierarchy) {
            int index = originElement.elementSiblingIndex();

            Elements children = nearestParent.children();
            if (index >= children.size()) {
                return nearestParent;
            }

            Element element = children.get(index);
            if (!element.tagName().equals(originElement.tagName())) {
                return nearestParent;
            }

            nearestParent = element;
        }

        return nearestParent;
    }

    private static class SimilarElement {
        private Element element;
        private int similarity;

        public SimilarElement(Element element, int similarity) {
            this.element = element;
            this.similarity = similarity;
        }

        public SimilarElement mostSimilar(SimilarElement other) {
            return (other.similarity > similarity) ? other : this;
        }
    }

}
