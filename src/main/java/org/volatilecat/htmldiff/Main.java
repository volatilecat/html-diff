package org.volatilecat.htmldiff;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.nio.charset.StandardCharsets;

public class Main {

    public static final String DEFAULT_TARGET_ID = "make-everything-ok-button";

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Usage: html-diff <origin-file> <file-to-analyze> [target-id]");
            return;
        }

        Document originalDoc = Jsoup.parse(new File(args[0]), StandardCharsets.UTF_8.name());
        Document docToAnalyze = Jsoup.parse(new File(args[1]), StandardCharsets.UTF_8.name());

        String targetId = (args.length > 2) ? args[2] : DEFAULT_TARGET_ID;
        Element target = originalDoc.getElementById(targetId);

        if (target == null) {
            System.out.println("Element with id " + targetId + " was not found in the origin file");
            return;
        }

        HtmlAnalyzer analyzer = new HtmlAnalyzer();
        Element element = analyzer.findMostSimilarElement(target, docToAnalyze);
        System.out.println(analyzer.buildPathToElement(element));
    }

}
