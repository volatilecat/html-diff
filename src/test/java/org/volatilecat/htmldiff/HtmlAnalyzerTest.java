package org.volatilecat.htmldiff;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HtmlAnalyzerTest {

    private HtmlAnalyzer analyzer = new HtmlAnalyzer();

    @Test
    public void shouldCompareAttributes() {
        Document doc1 = Jsoup.parse(
                "<html><body>" +
                        "<div> <a id=\"target\" class=\"a\" href=\"b\" title=\"c\">A</a> <div>" +
                        "</body></html>");

        Document doc2 = Jsoup.parse(
                "<html><body>" +
                        "<div>" +
                        " <a class=\"a\" href=\"z\">A</a>" +
                        " <a class=\"a\" href=\"b\" title=\"z\">A</a>" +
                        " <a class=\"b\" title=\"c\">A</a>" +
                        " <div>" +
                        "</body></html>");

        assertEquals("#root > html > body > div > a[2]", getResult(doc1, doc2));
    }

    @Test
    public void shouldCompareInnerText() {
        Document doc1 = Jsoup.parse(
                "<html><body>" +
                        "<div> <a id=\"target\">A</a> <div>" +
                        "</body></html>");

        Document doc2 = Jsoup.parse(
                "<html><body>" +
                        "<div> <a>B</a> <a>A<img></a> <a>C</a> <div>" +
                        "</body></html>");

        assertEquals("#root > html > body > div > a[2]", getResult(doc1, doc2));
    }

    @Test
    public void shouldCompareTagName() {
        Document doc1 = Jsoup.parse(
                "<html><body>" +
                        "<div> <a id=\"target\">A</a> <div>" +
                        "</body></html>");

        Document doc2 = Jsoup.parse(
                "<html><body>" +
                        "<div> <button>A</button> <a>A</a> <i>A</i> <div>" +
                        "</body></html>");

        assertEquals("#root > html > body > div > a", getResult(doc1, doc2));
    }

    @Test
    public void shouldPreferNearestElement() {
        Document doc1 = Jsoup.parse(
                "<html><body>" +
                        "<div> </div>" +
                        "<div> <a id=\"target\">A</a> <a>B</a> <div>" +
                        "<div> </div>" +
                        "</body></html>");

        Document doc2 = Jsoup.parse(
                "<html><body>" +
                        "<div> <a>A</a> </div>" +
                        "<div> <a>B</a> <a>A</a> <div>" +
                        "<div> <a>A</a> </div>" +
                        "</body></html>");

        assertEquals("#root > html > body > div[2] > a[2]", getResult(doc1, doc2));
    }

    private String getResult(Document origin, Document document) {
        Element target = origin.getElementById("target");
        return analyzer.buildPathToElement(
                analyzer.findMostSimilarElement(target, document));
    }

}
