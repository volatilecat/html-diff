html-diff
=========

The program analyzes two HTML files that might slightly differ, and tries to find some
specific element from the first file in the second file. By default, the program searches
for an element with id = `make-everything-ok-button`.

Usage
-----

```
$ java -jar html-diff.jar <origin-file> <file-to-analyze> [target-id]
```

`<origin-file>` - the path to the original file which contains the target element

`<file-to-analyze>` - the path to the modified file to search for a similar element

`[target-id]` - the (optional) id of the element to search for

Examples
--------

```
$ java -jar html-diff.jar samples/sample-0-origin.html samples/sample-1-evil-gemini.html
#root > html > body > div > div > div[3] > div[1] > div > div[2] > a[2]

$ java -jar html-diff.jar samples/sample-0-origin.html samples/sample-2-container-and-clone.html
#root > html > body > div > div > div[3] > div[1] > div > div[2] > div > a

$ java -jar html-diff.jar samples/sample-0-origin.html samples/sample-3-the-escape.html
#root > html > body > div > div > div[3] > div[1] > div > div[3] > a

$ java -jar html-diff.jar samples/sample-0-origin.html samples/sample-4-the-mash.html
#root > html > body > div > div > div[3] > div[1] > div > div[3] > a

$ java -jar html-diff.jar samples/sample-0-origin.html samples/sample-4-the-mash.html side-menu
#root > html > body > div > nav > div[2] > div > ul
```

Building
--------

```
$ mvn clean verify
```
